function [lambda, x, error] = sice(A, V, D, lambda, x, niter)
% SICE Improving the accuracy of computed eigenpair
% function [lambda, x] = sice(A, lambda, x, niter)
% Input lambda and x is the target eigenpair of matrix A.
% Output lambda and x is the improved eigenpair.
% niter is maximum number of iterations for refinement.
% niter is also optional. default = 5
% 
% First stage of the algorithm uses Matlab function schur
% to compute the schur decompition in single precision.
%
% The second stage is described in the reference:
% http://www.netlib.org/utk/people/JackDongarra/PAPERS/002_1982_algorithm-589-sicedr.pdf
%
% Implemented by: Yaohung Mike Tsai  <ytsai2@vols.utk.edu>
%
if nargin == 3
    niter = 5
end

n=size(A,1);
%[b i]=sort(abs(x));
%s=i(end);
%x=x/x(s); %normalize
%s

[m, s] = max(abs(x));
x=x/m;


% [Q, T]=schur(A);
%[S,B] = balance(A);
%[Q, T]=schur(single(A));
Q = V;
T = D;
result=zeros(1,niter+1);
normA=norm(A,inf);
for i=1:niter
    r=lambda*x-A*x; %residual vector
    error(i)=norm(r,inf)/normA/norm(x,inf);
    disp(['Norm of residual vector before iteration ', num2str(i), ' : ', num2str(norm(r))]);
    a_lambda_s = A(:,s); % eq (3.14)
    a_lambda_s(s) = a_lambda_s(s) - lambda;
    c=-x-a_lambda_s;
    c=c;
    d=Q'*c; % eq (3.15)
    fT=Q(s,:);
%    % Explicitly form matrix B
%    B=A-lambda*eye(n);
%    B(:,s)=B(:,s)+c;
%    y=B\r;
% .  % Explicitly form matrix T_lambda-d*fT
%    y=(T-lambda*eye(n)+d*fT)\(Q'*r);
%    y=Q*y;
    rhs = Q'*r;
    T_lambda=T-lambda*eye(n);
    % Apply Q_1
    for k=n:-1:2
        [P, z] = planerot(d(k-1:k)); % givens rotation
        %T_lambda(k-1:k,:) = P*T_lambda(k-1:k,:);
        T_lambda(k-1:k,k-1:n) = P*T_lambda(k-1:k,k-1:n);
        d(k-1:k) = z;
        rhs(k-1:k) = P * rhs(k-1:k);
    end
    %P
    %z
    T_lambda(1,:) = T_lambda(1,:) + d(1)*fT;
    % Apply Q_2
    for k=1:n-1
        [P, z] = planerot(T_lambda(k:k+1,k));
        %T_lambda(k:k+1,:) = P*T_lambda(k:k+1,:);
        T_lambda(k:k+1,k:n) = P*T_lambda(k:k+1,k:n);
        rhs(k:k+1) = P*rhs(k:k+1);
        T_lambda(k:k+1,k)=z;
        %T_lambda(k+1,k)=0; % forcing real zero
    end
    %P
    %z
    y=T_lambda\rhs; % T_lambda is upper triangular
    %y=(T-(lambda)*eye(n)+d*fT)\rhs;
    %y=gmres(T_lambda,rhs,5,1e-12,15,T_lambda);
    y=Q*y;
    new_x=x+y; % new eigenvector
    new_x(s)=x(s); % restore x(s) since y_s = 0
    disp(['Computed eigenvalue offset at  iteration ', num2str(i), ' : ', num2str(y(s))]);
    lambda=lambda+y(s); % new eigenvalue
    % convergence check: break if
    % 2*(previous correction) < current correction
    %if(i>=2 && abs(y(s))>2*abs(last_u))
    %    break
    %end
    last_u = y(s);
    x=new_x;
end
r=A*x - lambda*x;
error(niter+1)=norm(r)/normA/norm(x);
disp(['Norm of residual vector at the end : ', num2str(norm(r))]);
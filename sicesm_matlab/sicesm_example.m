rng(0);
n=100;
%A=rand(n);
%A=(A+A')/2;
%A=wilkinson(n);
%A=gallery('clement',n,1);
%A=gallery('dorr',n,0.1);
%A=full(A);
%A=gallery('lehmer',n);
A=gallery('randsvd',n,-1e7,3,n-1, n-1, 1);
%A=gallery('randsvd',n,-1/eps,3);
%A=gallery('randsvd',n,-100000,3);

%[V, D]=eig(single(A));
%V=double(V);
%D=double(D);
%i=11;
%x=double(V(:,i));
%lambda=double(D(i,i));
%x=rand(n,1);
%[new_lambda, new_x] = sice(A,lambda,x,10);
%[new_lambda, new_x] = sicesm(A,V,D,lambda,x,10);
%new_lambda

[Q, T]=hess(single(A));
Q=double(Q);
T=double(T);

[V, D]=eig(T);
V=Q*V;

[new_lambda, new_x] = sicesm(A,Q,T,lambda,x,10);
% try all eigenpairs and plot residual

max_it = 10;
result=zeros(max_it+1,n);
normA=norm(A);
for i=1:n
    x=double(V(:,i));
    lambda=double(D(i,i));
    [lambda, x, error] = sicesm(A, Q, T,lambda,x,max_it);
    result(:,i)=error';
end
semilogy(result)
xlabel('Iteration')
ylabel('Normalized Residual |Ax-\lambda x|/|A||x|')
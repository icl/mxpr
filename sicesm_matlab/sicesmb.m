function [lambda, X, error] = sicesmb(A, V, T, lambda, X, niter)
% SICE Improving the accuracy of computed eigenpair
% function [lambda, x] = sice(A, lambda, x, niter)
% Input lambda and x is the target eigenpair of matrix A.
% Output lambda and x is the improved eigenpair.
% niter is maximum number of iterations for refinement.
% niter is also optional. default = 5
% 
% First stage of the algorithm uses Matlab function schur
% to compute the schur decompition in single precision.
%
% The second stage is described in the reference:
% http://www.netlib.org/utk/people/JackDongarra/PAPERS/002_1982_algorithm-589-sicedr.pdf
%
% Implemented by: Yaohung Mike Tsai  <ytsai2@vols.utk.edu>
%
if nargin == 3
    niter = 5;
end

if nargin == 3
    s = 1;
end

[n, l]=size(X);
%s=1;

lambda=lambda+1e-10;
X=X+0.5*X*(eye(l)-X'*X);
error=[];
for i=1:niter
    s=i+1;
    R=X*diag(lambda)-A*X;
    %R=double(single(X)*diag(single(lambda))-single(A)*single(X));
    error=[error;vecnorm(R)];
    a=A(:,s);
    C=-X-repmat(a, 1, l);
    C(s,:) = C(s,:) + lambda';
    dd = double(single(V') * single(C));
    %dd = V'* C;
    fT=  V(s,:);
    rhs = double(single(V') * single(R));
    %rhs = V' * R;
    y=zeros(n, l);
    for j=1:l
        T_lambda=T-lambda(j)*eye(n);
        [L, U]=lu_nopiv(T_lambda);
        r=U\(L\rhs(:,j));
        d=U\(L\dd(:,j));
        y(:,j)=r - d*(fT*(r))/(1+fT*d);
    end
    y= double(single(V) * single(y));
    %(s,X(s,:)<1/n)=0;
    if( i~=1 )
        new_X=X+y; % new eigenvector
        new_X(s,:)=X(s,:); % restore x(s) since y_s = 0
        X=new_X;
    end
    disp(['Computed eigenvalue offset at  iteration ', num2str(i), ' : ', num2str(y(s,:))]);
    lambda=lambda+y(s,:)';
    for k=1:l, X(:,k) = X(:,k) / norm(X(:,k)); end;
    %E=eye(l)-X'*X;
    %E(abs(E)<1e-6)=0;
    %X=X+0.5*X*E;
end
X=X+0.5*X*(eye(l)-X'*X);
R=X*diag(lambda)-A*X;
error=[error;vecnorm(R)];
disp(['Norm of residual vector at the end : ', num2str(norm(R)/norm(A)/norm(X))]);
rng(0);
n=100;
%A=rand(n);
%A=2*rand(n)-1;
%A=(A+A')/2;
%A=wilkinson(n);
%A=gallery('randsvd',n,-1/eps,3);
A=gallery('randsvd',n,-1e4,3);
% last argument can be 1-5
% 1: One large singular value.
% 2: One small singular value.
% 3: Geometrically distributed singular values (default).
% 4: Arithmetically distributed singular values.
% 5: Random singular values with uniformly distributed logarithm.
[V,D] = eig(single(A));
V=double(V);
D=double(D);

[Q, T]=hess(single(A));
Q=double(Q);
T=double(T);

[V, D]=eig(T);
V=Q*V;

disp(['Norm of residual A*X-X*D at beginning      ' num2str(norm(A*V-V*D))]);
[X_ref,D_ref] = eig(A);
d_ref=diag(D_ref);

idx=[1:10];
l=size(idx,2);
X=V(:,idx);
lambda=D(idx,idx);
lambda=diag(lambda);
idx=1:l;
niter=20;
log=ones(n,niter+1);
results=ones(3,niter+1);

normA=norm(A);
for j=idx
    log(j,1) = norm(A*X(:,j)-lambda(j)*X(:,j)) /normA/norm(X(:,j));
end
results(1,1) = norm(A*X-X*diag(lambda))/normA;
results(2,1) = norm(X'*X-eye(l));
results(3,1) = norm(X'*A*X-diag(lambda));

for i=2:(niter+1)
    [lambda, X] = sicesmb(A, Q, T, lambda, X, 1);
    for j=idx
        log(j,i)= norm(A*X(:,j)-lambda(j)*X(:,j)) /normA/norm(X(:,j));
    end
    results(1,i) = norm(A*X-X*diag(lambda))/normA;
    results(2,i) = norm(X'*X-eye(l));
    tmp = X'*A*X;
    tmp = tmp - diag(diag(tmp));
    results(3,i) = norm(tmp);
    disp(['Norm of residual A*X-X*D after iteration ',num2str(i) ,' ' num2str(norm(A*X-X*diag(lambda))/normA)]);
    %if norm(A*X-X*D)<eps*cond(A)
    %    break
    %end
end

%for i=1:n
%    X(:,i) = X(:,i) / norm(X(:,i));
%end

h=semilogy(log(:,:)');
xlabel('Iteration');
ylabel('Normalized Residual');
ylabel('Residual |Ax-\lambda x|_\infty')
%h=semilogy(results');
%ylabel('2-Norm of Error Matrices')
%set(h,{'DisplayName'},{'norm(A*X-X*D)/norm(A)';'norm(I-X''*X)';'norm(offdiag(X''*A*X))'});
%legend show;



rng(0);
n=100;
%A=rand(n);
%A=(A+A')/2;
%A=wilkinson(n);
%A=gallery('clement',n,1);
%A=gallery('dorr',n,0.1);
%A=full(A);
%A=gallery('lehmer',n);
A=gallery('randsvd',n,-1e7,3,n-1, n-1, 1);
%A=gallery('randsvd',n,-1/eps,3);
%A=gallery('randsvd',n,-100000,3);

%[V, D]=eig(single(A));
%V=double(V);
%D=double(D);
%i=11;
%x=double(V(:,i));
%lambda=double(D(i,i));
%x=rand(n,1);
%[new_lambda, new_x] = sice(A,lambda,x,10);
%[new_lambda, new_x] = sicesm(A,V,D,lambda,x,10);
%new_lambda

[Q, T]=hess(single(A));
Q=double(Q);
T=double(T);

[V, D]=eig(T);
V=Q*V;

iter=20;
[lambda, X, error] = sicesmb(A*1000, Q, T*1000, diag(D)*1000, V, iter);
set(gcf, 'Position', [100 100 1000 600]);
linecolors = jet(n);
linecolors = linecolors(n:-1:1,:);
colororder(linecolors)
semilogy(0:iter, error,'LineWidth',.3)
grid on
xlabel('Iteration');
ylabel('Residual |Ax-\lambda x|_\infty')
exportgraphics(gcf,'sicesm_conv.png','Resolution',600)


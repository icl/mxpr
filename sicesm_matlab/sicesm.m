function [lambda, x, result] = sicesm(A, V, T, lambda, x, niter)
% SICE Improving the accuracy of computed eigenpair
% function [lambda, x] = sice(A, lambda, x, niter)
% Input lambda and x is the target eigenpair of matrix A.
% Output lambda and x is the improved eigenpair.
% niter is maximum number of iterations for refinement.
% niter is also optional. default = 5
% 
% First stage of the algorithm uses Matlab function schur
% to compute the schur decompition in single precision.
%
% The second stage is described in the reference:
% http://www.netlib.org/utk/people/JackDongarra/PAPERS/002_1982_algorithm-589-sicedr.pdf
%
% Implemented by: Yaohung Mike Tsai  <ytsai2@vols.utk.edu>
%
if nargin == 3
    niter = 5
end

n=size(A,1);
%[m s]=max(abs(x));
%[b i]=sort(abs(x));
%s=i(end+1-ids);
%x(s)
%s
%s=i(end);
%x=x/x(s); %normalize

%[m, s] = max(abs(x));
%x=x/m;

result=zeros(1,niter+1);
normA=norm(A);

lambda=lambda+1e-10;

for i=1:niter
    s=i;
    %while(x(s)<1e-2)
    %    s=mod(s+1,n);
    %end
    r=lambda*x-A*x; %residual vector
    disp(['Norm of residual vector before iteration ', num2str(i), ' : ', num2str(norm(r))]);
    result(i)=norm(r)/normA/norm(x);
    a_lambda_s = A(:,s); % eq (3.14)
    a_lambda_s(s) = a_lambda_s(s) - lambda;
    c=-x-a_lambda_s;
    d=V'*c; % eq (3.15)
    fT=V(s,:);
    rhs = V'*r;
    T_lambda=T-lambda*eye(n);
    [L U]=lu_nopiv(T_lambda);
    rhs=U\(L\rhs);
    d=U\(L\d);
    y=rhs - d*(fT*(rhs))/(1+fT*d);
    %y=(T-lambda*eye(n)+d*fT)\rhs;
    y=V*y;
    if( i~=1 )
        new_x=x+y; % new eigenvector
        new_x(s)=x(s); % restore x(s) since y_s = 0
        x=new_x;
    end
    disp(['Computed eigenvalue offset at  iteration ', num2str(i), ' : ', num2str(y(s))]);
    lambda=lambda+y(s); % new eigenvalue
    last_u = y(s);
    % convergence check: break if
    % 2*(previous correction) < current correction
    %if(i>=2 && abs(y(s))>2*abs(last_u))
    %    break
    %end
end
r=A*x - lambda*x;
result(niter+1)=norm(r)/normA/norm(x);
disp(['Norm of residual vector at the end : ', num2str(norm(r))]);
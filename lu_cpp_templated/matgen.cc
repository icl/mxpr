#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cmath>
#include "int-lu.h"

#include <cstdio>

#define STATE_INIT 1  // RNG initial state. Can be any odd number.

static uint64_t state = STATE_INIT;  // Not thread safe.

// Multiplicative Congruential Generator (MCG)
uint32_t mcg_rand() {
    const uint64_t MULTIPLIER = 14647171131086947261ULL;
    state *= MULTIPLIER;
    return state >> 32; /* use high 32 bits */
}

// Jump ahead function to go through N steps in log(N) time.
void mcg_advance(unsigned int delta) {
    const uint64_t MULTIPLIER = 14647171131086947261ULL;
    uint64_t accum = MULTIPLIER;
    while (delta != 0) {
        if (delta & 1) {
            delta = delta - 1;
            state *= accum;
        }
        delta = delta / 2;
        accum = accum * accum;
    }
}

// Reset RNG state.
void mcg_reset() { state = STATE_INIT; }

// Generate double floating-point number from uniform(-0.5, 0.5)
double mcg_rand_double() { return ((double)mcg_rand()) / UINT32_MAX - 0.5; }

// Generate a row diagonally dominant square matrix A.
template <typename T>
void matgen(T* A, int lda, int m, int n) {

    int i, j;

    // double *diag = (double *)malloc(m * sizeof(double));
    // memset(diag, 0, m * sizeof(double));

    for (j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            A(i, j) = mcg_rand_double();
            // diag[i] += fabs(A(i, j));
        }
    }

    // for (i = 0; i < m; i++) {
    //    A(i, i) = diag[i] - fabs(A(i, i));
    // }

    // free(diag);
}

template <typename T>
void vecgen(T* v, int n) {
    int i;
    for (i = 0; i < n; i++) {
        v[i] = mcg_rand_double();
    }
    return;
}

template void matgen<double>(double* A, int lda, int m, int n);
template void matgen<float>(float* A, int lda, int m, int n);
template void vecgen<double>(double* v, int n);
template void vecgen<float>(float* v, int n);

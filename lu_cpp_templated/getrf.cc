#include <cstdio>
#include <cmath>
#include "int-lu.h"

template <typename T>
void getrf(int m, int n, T *A, int lda, int *ipiv, int &info) {

    int j;
    int nb = 256;
    int jb = nb;
    int iinfo;
    int min_mn = m < n ? m : n;

    // Use unblock code.
    if (nb <= 1 || nb >= min_mn) {
        getrf2<T>(m, n, A, lda, ipiv, info);
        return;
    }

    for (j = 1; j <= min_mn; j += nb) {
        if (min_mn - j + 1 < nb) {
            jb = min_mn - j + 1;
        }

        // Factor panel
        getrf2<T>(m - j + 1, jb, &A(j - 1, j - 1), lda, ipiv + j - 1, iinfo);

        // Adjust pivot indices
        for (int i = j - 1; i < m && i < j + jb - 1; i++) {
            ipiv[i] += j - 1;
        }

        laswp<T>(j - 1, A, lda, j, j + jb - 1, ipiv, 1);

        if (j + jb <= n) {
            laswp<T>(n - j - jb + 1, &A(0, j - 1 + jb), lda, j, j + jb - 1,
                     ipiv, 1);

            trsm<T>('L', 'L', 'N', 'U', jb, n - j - jb + 1, 1.0,
                    &A(j - 1, j - 1), lda, &A(j - 1, j - 1 + jb), lda);

            if (j + jb <= m) {
                gemm<T, T>('N', 'N', m - j - jb + 1, n - j - jb + 1, jb, -1.0,
                           &A(j - 1 + jb, j - 1), lda, &A(j - 1, j - 1 + jb),
                           lda, 1.0, &A(j - 1 + jb, j - 1 + jb), lda);
            }
        }
    }
}

template <typename T>
void getrf_ll(int m, int n, T *A, int lda, int *ipiv, int &info) {

    int j;
    int nb = 256;
    int jb = nb;
    int iinfo;
    int min_mn = m < n ? m : n;

    // Use unblock code.
    if (nb <= 1 || nb >= min_mn) {
        getrf2<T>(m, n, A, lda, ipiv, info);
        return;
    }

    for (j = 1; j <= min_mn; j += nb) {
        if (min_mn - j + 1 < nb) {
            jb = min_mn - j + 1;
        }

        // Update before factoring the current panel
        for (int k = 1; k <= j - nb; k += nb) {

            // Apply interchanges to rows K:K+NB-1.
            laswp<T>(jb, &A(0, j - 1), lda, k, k + nb - 1, ipiv, 1);

            // Compute block row of U.
            trsm<T>('L', 'L', 'N', 'U', nb, jb, 1.0, &A(k - 1, k - 1), lda,
                    &A(k - 1, j - 1), lda);

            // Update trailing submatrix.
            gemm<T, T>('N', 'N', m - k - nb + 1, jb, nb, -1.0,
                       &A(k + nb - 1, k - 1), lda, &A(k - 1, j - 1), lda, 1.0,
                       &A(k + nb - 1, j - 1), lda);
        }

        // Factor panel
        getrf2<T>(m - j + 1, jb, &A(j - 1, j - 1), lda, ipiv + j - 1, iinfo);

        // Adjust pivot indices
        for (int i = j - 1; i < m && i < j + jb - 1; i++) {
            ipiv[i] += j - 1;
        }
    }

    for (int k = 1; k <= min_mn; k += nb) {
        int end = min_mn;
        if (k + nb - 1 < min_mn) {
            end = k + nb - 1;
        }
        laswp<T>(k - 1, A, lda, k, end, ipiv, 1);
    }

    // Apply update to the M+1:N columns when N > M
    if (n > m) {
        laswp<T>(n - m, &A(0, m), lda, 1, m, ipiv, 1);

        for (int k = 1; k <= m; k += nb) {
            jb = nb;
            if (m - k + 1 < nb) {
                jb = m - k + 1;
            }

            trsm<T>('L', 'L', 'N', 'U', jb, n - m, 1.0, &A(k - 1, k - 1), lda,
                    &A(k - 1, m), lda);

            if (k + nb <= m) {
                gemm<T, T>('N', 'N', m - k - nb + 1, n - m, nb, -1.0,
                           &A(k + nb - 1, k - 1), lda, &A(k - 1, m), lda, 1.0,
                           &A(k + nb - 1, m), lda);
            }
        }
    }
}

template <typename T>
void getrf2(int m, int n, T *A, int lda, int *ipiv, int &info) {

    if (m <= 1 || n == 0) {
        ipiv[0] = 1;
        if (*A == 0.0) {
            info = 1;
        }
        return;
    }

    if (n == 1) {

        int i = iamax<T>(m, A, 1);
        ipiv[0] = i;

        if (A(i - 1, 0) != 0) {
            if (i != 1) {
                T temp = A(0, 0);
                A(0, 0) = A(i - 1, 0);
                A(i - 1, 0) = temp;
            }

            T scale = 1.0 / A(0, 0);
            for (int j = 1; j < m; j++) {
                A(j, 0) *= scale;
            }

        } else {
            info = 1;
        }

    } else {  // Use recursive code

        int n1 = (m > n ? n : m) / 2;
        int n2 = n - n1;

        getrf2<T>(m, n1, A, lda, ipiv, info);

        laswp<T>(n2, &A(0, n1), lda, 1, n1, ipiv, 1);

        trsm<T>('L', 'L', 'N', 'U', n1, n2, 1.0, A, lda, &A(0, n1), lda);

        gemm<T, T>('N', 'N', m - n1, n2, n1, -1.0, &A(n1, 0), lda, &A(0, n1),
                   lda, 1.0, &A(n1, n1), lda);

        getrf2<T>(m - n1, n2, &A(n1, n1), lda, ipiv + n1, info);

        // Adjust pivot indices
        int min_mn = m < n ? m : n;
        for (int i = n1; i < min_mn; i++) {
            ipiv[i] += n1;
        }

        laswp<T>(n1, A, lda, n1 + 1, min_mn, ipiv, 1);
    }
    return;
}

template <typename T>
void laswp(int n, T *A, int lda, int k1, int k2, int *ipiv, int incx) {
    int ix, ip, ix0, i1, i2, inc;
    T temp;

    if (incx > 0) {
        ix0 = k1;
        i1 = k1;
        i2 = k2;
        inc = 1;
    } else if (incx < 0) {
        ix0 = k1 + (k1 - k2) * incx;
        i1 = k2;
        i2 = k1;
        inc = -1;
    } else {
        return;
    }

    int n32 = (n / 32) * 32;

    if (n32 != 0) {
        for (int j = 0; j < n32; j += 32) {
            ix = ix0 - 1;
            for (int i = i1 - 1; i != i2; i += inc) {
                ip = ipiv[ix] - 1;
                if (ip != i) {
                    for (int k = j; k < j + 32; k++) {
                        temp = A(i, k);
                        A(i, k) = A(ip, k);
                        A(ip, k) = temp;
                    }
                }
                ix += incx;
            }
        }
    }

    if (n32 != n) {
        ix = ix0 - 1;
        for (int i = i1 - 1; i != i2; i += inc) {
            ip = ipiv[ix] - 1;
            if (ip != i) {
                for (int k = n32; k < n; k++) {
                    temp = A(i, k);
                    A(i, k) = A(ip, k);
                    A(ip, k) = temp;
                }
            }
            ix += incx;
        }
    }

    return;
}

template <typename T>
int iamax(int n, T *sx, int incx) {
    int isamax = 1;
    if (n < 1 || incx <= 0) {
        return 0;
    }
    if (n == 1) {
        return 1;
    }
    T smax = fabs(sx[0]);
    if (incx == 1) {
        for (int i = 1; i < n; i++) {
            if (fabs(sx[i]) > smax) {
                isamax = i + 1;
                smax = fabs(sx[i]);
            }
        }
    } else {
        for (int i = incx; i < n * incx; i += incx) {
            if (fabs(sx[i]) > smax) {
                isamax = i + 1;
                smax = fabs(sx[i]);
            }
        }
    }
    return isamax;
}

template void getrf<float>(int m, int n, float *A, int lda, int *ipiv,
                           int &info);
template void getrf<double>(int m, int n, double *A, int lda, int *ipiv,
                            int &info);

template void getrf_ll<float>(int m, int n, float *A, int lda, int *ipiv,
                              int &info);
template void getrf_ll<double>(int m, int n, double *A, int lda, int *ipiv,
                               int &info);

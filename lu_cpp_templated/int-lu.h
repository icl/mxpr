#pragma once
#include <cstdint>
#include <cmath>

#define INDEX2D(PTR, R, C, LDIM) \
    (((PTR) + (R)) + sizeof(char) * (C) / sizeof(char) * (LDIM))
#define A(i, j) *INDEX2D(A, (i), (j), lda)
#define B(i, j) *INDEX2D(B, (i), (j), ldb)
#define C(i, j) *INDEX2D(C, (i), (j), ldc)
#define A_int(i, j) *INDEX2D(A_int, (i), (j), lda)

uint32_t mcg_rand(void);
double mcg_rand_double(void);
void mcg_advance(unsigned int delta);
void mcg_reset(void);
template <typename T>
void matgen(T *A, int lda, int m, int n);
template <typename T>
void vecgen(T *v, int n);
double get_wtime(void);
template <typename T>
void print_matrix(T *A, int lda, int m, int n);
template <typename srcType, typename dstType>
void convert(srcType *src, int ldsrc, dstType *dst, int lddst, int m, int n);

template <typename T>
void getrf(int m, int n, T *A, int lda, int *ipiv, int &info);
template <typename T>
void getrf_ll(int m, int n, T *A, int lda, int *ipiv, int &info);
template <typename T>
void getrf_ll(int m, int n, T *A, int lda, int *ipiv, T scale_L, T scale_U, int &info);
template <typename T>
void getrf2(int m, int n, T *A, int lda, int *ipiv, int &info);

double dlange(char norm, int m, int n, double *A, int lda);
void dgemv(char trans, int m, int n, double alpha, double *A, int lda,
           double *X, int incx, double beta, double *Y, int incy);

template <typename InType, typename OutType>
void gemm(char transa, char transb, int m, int n, int k, InType alpha,
          InType *A, int lda, InType *B, int ldb, InType beta, OutType *C,
          int ldc);

void sgemm(char transa, char transb, int m, int n, int k, float alpha, float *A,
           int lda, float *B, int ldb, float beta, float *C, int ldc);

template <typename T>
void trsm(char side, char uplo, char transa, char diag, int m, int n, T alpha,
          T *A, int lda, T *B, int ldb);

void strsm(char side, char uplo, char transa, char diag, int m, int n,
           float alpha, float *A, int lda, float *B, int ldb);

template <typename T>
void laswp(int n, T *A, int lda, int k1, int k2, int *ipiv, int incx);

template <typename T>
int iamax(int n, T *sx, int incx);
int isamax(int n, float *sx, int incx);

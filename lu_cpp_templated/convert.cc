#include "int-lu.h"

#define S(i, j) *INDEX2D(src, (i), (j), ldsrc)
#define D(i, j) *INDEX2D(dst, (i), (j), lddst)

template <typename srcType, typename dstType>
void convert(srcType* src, int ldsrc, dstType* dst, int lddst, int m, int n) {
    int i, j;
    for (i = 0; i < m; ++i) {
        for (j = 0; j < n; ++j) {
            D(i, j) = (dstType)S(i, j);
        }
    }
    return;
}

template void convert<double, float>(double*, int, float*, int, int, int);
template void convert<float, double>(float*, int, double*, int, int, int);

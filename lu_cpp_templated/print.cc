#include <cstdio>
#include <typeinfo>
#include "int-lu.h"

template <typename T>
void print_matrix(T* A, int lda, int m, int n) {

    int i, j;
    if (lda < m) {
        return;
    }
    const char* print_format;
    if (typeid(T) == typeid(int)) {
        print_format = " %6d";
    } else if (typeid(T) == typeid(float)) {
        print_format = " %10.6f";
    } else {
        print_format = " %14.10f";
    }
    printf("[%s", m == 1 ? " " : "\n");
    for (i = 0; i < m; ++i) {
        for (j = 0; j < n; ++j) {
            printf(print_format, A(i, j));
        }
        printf("%s", m > 1 ? "\n" : " ");
    }
    printf("];\n");
    return;
}

template void print_matrix<float>(float* A, int lda, int m, int n);
template void print_matrix<double>(double* A, int lda, int m, int n);
template void print_matrix<int>(int* A, int lda, int m, int n);

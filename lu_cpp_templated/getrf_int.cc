#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "int-lu.h"

// This is the interface routine which does the scaling and casting.
void getrf_int32(int m, int n, double *A, int lda, int *ipiv, int &info) {
     double scale_L_double = pow(2, 30);
     double scale_U_double = pow(2, 22);
     int32_t scale_L_int = int32_t(scale_L_double);
     int32_t scale_U_int = int32_t(scale_U_double);

     int32_t* A_int = (int32_t*) malloc(m*lda*sizeof(int32_t));

     // Scale
     double alpha = 0.0;
     for(int i=0; i<m; i++) {
         for(int j=0; j<n; j++) {
             if( fabs(A(i, j))>alpha ) {
                 alpha = fabs(A(i, j));
             }
         }
     }

     for(int i=0; i<m; i++) {
         for(int j=0; j<n; j++) {
             double temp = A(i, j) / alpha;
             A_int(i, j) = int32_t( temp * scale_U_double );
         }
     }

     getrf_ll<int32_t>(m, n, A_int, lda, ipiv, scale_L_int, scale_U_int, info);

     for(int i=0; i<m; i++) {
         for(int j=0; j<n; j++) {
             if( i>j ) { // L
                 A(i, j) = double(A_int(i, j)) / scale_L_double;
             } else { // U
                 A(i, j) = double(A_int(i, j)) / scale_U_double * alpha;
             }
         }
     }         

     free(A_int);

     return;

}


template <typename T>
void getrf_ll(int m, int n, T *A, int lda, int *ipiv, T scale_L, T scale_U, int &info) {
    int j;
    int nb = 256;
    int jb = nb;
    int iinfo;
    int min_mn = m < n ? m : n;

    // Use unblock code.
    if (nb <= 1 || nb >= min_mn) {
        getrf2<T>(m, n, A, lda, ipiv, info);
        return;
    }

    for (j = 1; j <= min_mn; j += nb) {
        if (min_mn - j + 1 < nb) {
            jb = min_mn - j + 1;
        }

        // Update before factoring the current panel
        for (int k = 1; k <= j - nb; k += nb) {

            // Apply interchanges to rows K:K+NB-1.
            laswp<T>(jb, &A(0, j - 1), lda, k, k + nb - 1, ipiv, 1);

            // Compute block row of U.
            trsm<T>('L', 'L', 'N', 'U', nb, jb, 1.0, &A(k - 1, k - 1), lda,
                    &A(k - 1, j - 1), lda);

            // Update trailing submatrix.
            gemm<T, T>('N', 'N', m - k - nb + 1, jb, nb, -1.0,
                       &A(k + nb - 1, k - 1), lda, &A(k - 1, j - 1), lda, 1.0,
                       &A(k + nb - 1, j - 1), lda);
        }

        // Factor panel
        getrf2<T>(m - j + 1, jb, &A(j - 1, j - 1), lda, ipiv + j - 1, iinfo);

        // Adjust pivot indices
        for (int i = j - 1; i < m && i < j + jb - 1; i++) {
            ipiv[i] += j - 1;
        }
    }

    for (int k = 1; k <= min_mn; k += nb) {
        int end = min_mn;
        if (k + nb - 1 < min_mn) {
            end = k + nb - 1;
        }
        laswp<T>(k - 1, A, lda, k, end, ipiv, 1);
    }

    // Apply update to the M+1:N columns when N > M
    if (n > m) {
        laswp<T>(n - m, &A(0, m), lda, 1, m, ipiv, 1);

        for (int k = 1; k <= m; k += nb) {
            jb = nb;
            if (m - k + 1 < nb) {
                jb = m - k + 1;
            }

            trsm<T>('L', 'L', 'N', 'U', jb, n - m, 1.0, &A(k - 1, k - 1), lda,
                    &A(k - 1, m), lda);

            if (k + nb <= m) {
                gemm<T, T>('N', 'N', m - k - nb + 1, n - m, nb, -1.0,
                           &A(k + nb - 1, k - 1), lda, &A(k - 1, m), lda, 1.0,
                           &A(k + nb - 1, m), lda);
            }
        }
    }

}

template <typename T>
void getrf2(int m, int n, T *A, int lda, int *ipiv, T scale_L, T scale_U, int &info) {

    if (m <= 1 || n == 0) {
        ipiv[0] = 1;
        if (*A == 0.0) {
            info = 1;
        }
        return;
    }

    if (n == 1) {

        int i = iamax<T>(m, A, 1);
        ipiv[0] = i;

        if (A(i - 1, 0) != 0) {
            if (i != 1) {
                T temp = A(0, 0);
                A(0, 0) = A(i - 1, 0);
                A(i - 1, 0) = temp;
            }

            T scale = 1.0 / A(0, 0);
            for (int j = 1; j < m; j++) {
                A(j, 0) *= scale;
            }

        } else {
            info = 1;
        }

    } else {  // Use recursive code

        int n1 = (m > n ? n : m) / 2;
        int n2 = n - n1;

        getrf2<T>(m, n1, A, lda, ipiv, info);

        laswp<T>(n2, &A(0, n1), lda, 1, n1, ipiv, 1);

        trsm<T>('L', 'L', 'N', 'U', n1, n2, 1.0, A, lda, &A(0, n1), lda);

        gemm<T, T>('N', 'N', m - n1, n2, n1, -1.0, &A(n1, 0), lda, &A(0, n1),
                   lda, 1.0, &A(n1, n1), lda);

        getrf2<T>(m - n1, n2, &A(n1, n1), lda, ipiv + n1, info);

        // Adjust pivot indices
        int min_mn = m < n ? m : n;
        for (int i = n1; i < min_mn; i++) {
            ipiv[i] += n1;
        }

        laswp<T>(n1, A, lda, n1 + 1, min_mn, ipiv, 1);
    }
    return;
}

template <typename T>
int iamax(int n, T *sx, int incx) {
    int isamax = 1;
    if (n < 1 || incx <= 0) {
        return 0;
    }
    if (n == 1) {
        return 1;
    }
    T smax = fabs(sx[0]);
    if (incx == 1) {
        for (int i = 1; i < n; i++) {
            if (fabs(sx[i]) > smax) {
                isamax = i + 1;
                smax = fabs(sx[i]);
            }
        }
    } else {
        for (int i = incx; i < n * incx; i += incx) {
            if (fabs(sx[i]) > smax) {
                isamax = i + 1;
                smax = fabs(sx[i]);
            }
        }
    }
    return isamax;
}

template void getrf_ll<int32_t>(int m, int n, int32_t *A, int lda, int *ipiv, int32_t scale_L, int32_t scale_U, int &info);
template void getrf_ll<int16_t>(int m, int n, int16_t *A, int lda, int *ipiv, int16_t scale_L, int16_t scale_U, int &info);

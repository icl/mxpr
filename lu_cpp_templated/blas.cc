#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cmath>
#include "int-lu.h"

template <typename InType, typename OutType>
void gemm(char transa, char transb, int m, int n, int k, InType alpha,
          InType *A, int lda, InType *B, int ldb, InType beta, OutType *C,
          int ldc) {
    int i, j, l;

    // Only supprt transa=='N', trabsb=='N'
    if (transa != 'N' || transb != 'N') {
        printf("Not supported in SGEMM.\n");
        return;
    }

    if (m == 0 || n == 0) {
        return;
    }

    if ((alpha == 0.0 || k == 0) && beta == 1.0) {
        return;
    }

    // divide into 2 GEMMs over the maximum dimension of m, n, k.
    /*const int blocksize = 64;
    if( m >= blocksize && n >= blocksize && k >= blocksize ) {
    if ( m>=n && m>=k && m>blocksize ) {
        //printf("Recursive m %d %d %d\n", m, n, k);
        const int m1 = m / 2;
        const int m2 = m - m1;
        gemm<InType, OutType>(transa, transb, m1, n, k, alpha, A, lda, B, ldb,
                              beta, C, ldc);
        gemm<InType, OutType>(transa, transb, m2, n, k, alpha, &A(m1, 0), lda,
    B, ldb,
                              beta, &C(m1, 0), ldc);
        return;
    } else if ( n>=m && n>=k && n>blocksize ) {
        //printf("Recursive n %d %d %d\n", m, n, k);
        const int n1 = n / 2;
        const int n2 = n - n1;
        gemm<InType, OutType>(transa, transb, m, n1, k, alpha, A, lda, B, ldb,
                              beta, C, ldc);
        gemm<InType, OutType>(transa, transb, m, n2, k, alpha, A, lda, &B(0,
    n1), ldb,
                              beta, &C(0, n1), ldc);
        return;
    } else if ( k>=m && k>=n && k>blocksize) {
        //printf("Recursive k %d %d %d\n", m, n, k);
        const int k1 = k / 2;
        const int k2 = k - k1;
         gemm<InType, OutType>(transa, transb, m, n, k1, alpha, A, lda, B, ldb,
                               beta, C, ldc);
         gemm<InType, OutType>(transa, transb, m, n, k2, alpha, &A(0, k1), lda,
    &B(k1, 0), ldb,
                                                beta, C, ldc);
         return;
    } }*/

    if (alpha == 0.0) {
        if (beta == 0.0) {
            for (j = 0; j < n; j++) {
                for (i = 0; i < m; i++) {
                    C(i, j) = 0.0;
                }
            }
        } else {
            for (j = 0; j < n; j++) {
                for (i = 0; i < m; i++) {
                    C(i, j) = beta * C(i, j);
                }
            }
        }
    }

    for (j = 0; j < n; j++) {
        if (beta == 0.0) {
            for (i = 0; i < m; i++) {
                C(i, j) = 0.0;
            }
        } else {
            for (i = 0; i < m; i++) {
                C(i, j) = beta * C(i, j);
            }
        }
        for (l = 0; l < k; l++) {
            OutType temp = alpha * B(l, j);
            for (i = 0; i < m; i++) {
                C(i, j) += temp * A(i, l);
            }
        }
    }
    return;
}

void sgemm(char transa, char transb, int m, int n, int k, float alpha, float *A,
           int lda, float *B, int ldb, float beta, float *C, int ldc) {

    gemm<float, float>(transa, transb, m, n, k, alpha, A, lda, B, ldb, beta, C,
                       ldc);
}

template <typename T>
void trsm(char side, char uplo, char transa, char diag, int m, int n, T alpha,
          T *A, int lda, T *B, int ldb) {

    int i, j, k;

    // Only support side=='L', transa=='N', alpha==1.0.
    if (side != 'L' || transa != 'N' || alpha != 1.0) {
        printf("Not supported in STRSM.\n");
        return;
    }

    if (m == 0 || n == 0) {
        return;
    }

    int nounit = diag == 'N';

    if (uplo == 'U') {
        for (j = 0; j < n; j++) {
            for (k = m - 1; k >= 0; k--) {
                if (nounit) {
                    B(k, j) = B(k, j) / A(k, k);
                }
                for (i = 0; i < k; i++) {
                    B(i, j) = B(i, j) - B(k, j) * A(i, k);
                }
            }
        }
    } else {
        for (j = 0; j < n; j++) {
            for (k = 0; k < m; k++) {
                if (nounit) {
                    B(k, j) = B(k, j) / A(k, k);
                }
                for (i = k + 1; i < m; i++) {
                    B(i, j) = B(i, j) - B(k, j) * A(i, k);
                }
            }
        }
    }
    return;
}

void strsm(char side, char uplo, char transa, char diag, int m, int n,
           float alpha, float *A, int lda, float *B, int ldb) {
    trsm<float>(side, uplo, transa, diag, m, n, alpha, A, lda, B, ldb);
}

double dlange(char norm, int m, int n, double *A, int lda) {
    int i, j;

    // Frobenius norm
    if (norm == 'F') {
        double sum = 0.0;
        for (j = 0; j < n; ++j) {
            for (i = 0; i < m; ++i) {
                sum += A(i, j) * A(i, j);
            }
        }
        return sqrt(sum);
        // Infinity norm
    } else if (norm == 'I') {
        double *work = (double *)malloc(m * sizeof(double));
        memset(work, 0, m * sizeof(double));
        double max = 0.0;
        for (j = 0; j < n; ++j) {
            for (i = 0; i < m; ++i) {
                work[i] += fabs(A(i, j));
            }
        }
        for (i = 0; i < m; ++i) {
            if (max < work[i]) {
                max = work[i];
            }
        }
        free(work);
        return max;
    }
    return 0;
}

void dgemv(char trans, int m, int n, double alpha, double *A, int lda,
           double *X, int incx, double beta, double *Y, int incy) {
    int i, j;
    if (trans != 'N' || incx != 1 || incy != 1) {
        return;
    }

    if (beta != 1.0) {
        if (beta == 0.0) {
            for (i = 0; i < m; ++i) {
                Y[i] = 0;
            }
        } else {
            for (i = 0; i < m; ++i) {
                Y[i] = beta * Y[i];
            }
        }
    }

    if (alpha == 0.0) {
        return;
    }

    for (j = 0; j < n; ++j) {
        double temp = alpha * X[j];
        for (i = 0; i < m; ++i) {
            Y[i] += temp * A(i, j);
        }
    }
    return;
}

template void gemm<int, int>(char transa, char transb, int m, int n, int k,
                             int alpha, int *A, int lda, int *B, int ldb,
                             int beta, int *C, int ldc);

template void gemm<double, double>(char transa, char transb, int m, int n,
                                   int k, double alpha, double *A, int lda,
                                   double *B, int ldb, double beta, double *C,
                                   int ldc);
template void trsm<double>(char side, char uplo, char transa, char diag, int m,
                           int n, double alpha, double *A, int lda, double *B,
                           int ldb);

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "int-lu.h"

int main(int argc, char* argv[]) {
    int n = 100;  // matrix size
    if (argc >= 2) {
        n = atoi(argv[1]);
    }
    int info = 0;

    double time_solve;

    int lda = (n + 16 - 1) / 16 * 16;  // round up to multiple of 16

    double* A = (double*)malloc(lda * n * sizeof(double));
    double* LU = (double*)malloc(lda * n * sizeof(double));
    double* b = (double*)malloc(n * sizeof(double));
    double* x = (double*)malloc(n * sizeof(double));
    float* sA = (float*)malloc(lda * n * sizeof(float));
    float* sb = (float*)malloc(n * sizeof(float));
    int* ipiv = (int*)malloc(n * sizeof(int));

    matgen<double>(A, lda, n, n);
    vecgen<double>(b, n);

    for( int i=0; i<n; i++ ) {
        for( int j=0; j<n; j++ ) {
            //A[i*lda+j] = A[i*lda+j] + 0.5;
            //A[i*lda+j] *= 2^10;
            //A[i*lda+j] *= 2;
            
        }
    }

    memcpy(LU, A, lda * n * sizeof(double));

    time_solve = get_wtime();
    /*convert<double, float>(A, lda, sA, lda, n, n);
    convert<double, float>(b, n, sb, n, n, 1);

    getrf<float>(n, n, sA, lda, ipiv, info);

    convert<float, double>(sA, lda, LU, lda, n, n);*/

    getrf<double>(n, n, LU, lda, ipiv, info);

    time_solve = get_wtime() - time_solve;
    memcpy(x, b, n * sizeof(double));
    laswp<double>(1, x, n, 1, n, ipiv, 1);
    trsm<double>('L', 'L', 'N', 'U', n, 1, 1.0, LU, lda, x, n);
    trsm<double>('L', 'U', 'N', 'N', n, 1, 1.0, LU, lda, x, n);

    double norm_A = dlange('I', n, n, A, lda);
    double norm_x = dlange('I', n, 1, x, n);
    double norm_b = dlange('I', n, 1, b, n);
    dgemv('N', n, n, 1.0, A, lda, x, 1, -1.0, b, 1);

    double error = dlange('I', n, 1, b, n) / (norm_A * norm_x + norm_b) / n;

    printf("%e %e %e %e\n", dlange('I', n, 1, b, n), norm_A, norm_x, norm_b);

    printf(
        "||Ax-b||_oo / ( || x ||_oo * || A ||_oo + || b ||_oo ) * N "
        ")\n");
    printf("%e\n", error);

    printf("Time        : %12.3f second\n", time_solve);
    printf("Performance : %12.3f GFLOP/s\n",
           2.0 / 3.0 * n * n * n / 1000000000.0 / time_solve);

    //print_matrix<double>(A, lda+1, 1, n);
    //print_matrix<double>(LU, lda, n, n);
    //print_matrix<double>(A, lda, n, n);

    double max = 0.0;
    int maxi = 0;
    int maxj = 0;

    for( int i=0; i<n; i++ ) {
        for( int j=0; j<n; j++ ) {
            if( fabs(LU[i*lda+j])>max ) {
                max = fabs(LU[i*lda+j]);
                maxi = i;
                maxj = j;
            }
        }
    }

    printf("max %d %d = %.8f\n", maxi, maxj, max);
    
    free(A);
    free(LU);
    free(b);
    free(x);
    free(sA);
    free(sb);
    free(ipiv);

    return 0;
}

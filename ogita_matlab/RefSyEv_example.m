n=1000;
A=rand(n);
A=(A+A')/2;
%A=wilkinson(n);
%A=gallery('randsvd',n,-1/eps,3);
% last argument can be 1-5
% 1: One large singular value.
% 2: One small singular value.
% 3: Geometrically distributed singular values (default).
% 4: Arithmetically distributed singular values.
% 5: Random singular values with uniformly distributed logarithm.
[X,D] = eig(single(A));
X=double(X);
D=double(D);
disp(['Norm of residual A*X-X*D at beginning      ' num2str(norm(A*X-X*D))]);
[X_ref,D_ref] = eig(A);
d_ref=diag(D_ref);

idx=1:1000;
l=size(idx,2);
X=X(:,idx);
niter=10;
log=zeros(l,niter+1);

D=D(idx,idx);
log(:,1)=abs(diag(D)-d_ref(idx))./abs(d_ref(idx));

for i=2:(niter+1)
    [X, D, E, delta] = RefSyEv(A, X);
    log(:,i)=abs(diag(D)-d_ref(idx))./abs(d_ref(idx));
    norm(X'*X-eye(l))
    disp(['Norm of residual A*X-X*D after iteration ',num2str(i) ,' ' num2str(norm(A*X-X*D))]);
    %if norm(A*X-X*D)<eps*cond(A)
    %    break
    %end
end

semilogy(log(:,:)')
xlabel('Iteration')
ylabel('Relative error')

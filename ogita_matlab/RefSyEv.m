function [X, D, E, delta] = RefSyEv(A, X)
% REFSYEV Iterative refinement of eigenvalues and vectors
% function [X, D, E, delta] = RefSyEv(A, X)
% Input symmetric matrix A for the eigen problem A*X=X*D.
% Input X is the initial values of eigenvectors X.
% 
% It follows the Algorithm 1 of the paper:
% https://link.springer.com/article/10.1007/s13160-018-0310-3
%
% Implemented by: Yaohung Mike Tsai  <ytsai2@vols.utk.edu>
%
n=size(X,1);
l=size(X,2);
R=eye(l)-X'*X;
S=X'*A*X;
lambda=diag(S)./(1-diag(R));
D=diag(lambda);
delta=2*(norm(S-D)+norm(A)*norm(R)); % 2-norm
E=R/2;
for i=1:l
    for j=1:l
        if abs(D(i,i)-D(j,j)) > delta
            E(i,j)=(S(i,j)+D(j,j)*R(i,j)) / (D(j,j)-D(i,i));
        end
    end
end
X=X+X*E;
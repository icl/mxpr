function [L, U, P]=lu_test(A)

n=size(A,1);
P=eye(n);
for i=1:n
    [~, j] = max(abs(A(i:n,i)));
    j=j+i-1;
    j=i;
    
    %swap rows
    if i~=j
        temp = A(i,:);
        A(i,:) = A(j,:);
        A(j,:) = temp;
        temp = P(i,:);
        P(i,:) = P(j,:);
        P(j,:) = temp;
    end
    
    A(i+1:n, i) = A(i+1:n, i) ./ A(i, i);
    
    A(i+1:n, i+1:n) = A(i+1:n, i+1:n) - A(i+1:n, i) * A(i, i+1:n);
    
end

L = eye(n)+tril(A,-1);
U = triu(A);

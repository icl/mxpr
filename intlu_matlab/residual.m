rng(0);
n=1000;
A=gallery('randsvd',n,1e4,3,n-1, n-1, 1);
A=A/(max(max(A)));
b=rand(n,1);
cond_A = cond(A)
[L_ref U_ref P_ref] = lu(A);
max(max(abs(U_ref)))

x =  U_ref\(L_ref\(P_ref*b));
r = b-A*x;
norm_ref = norm(r, inf)


t = tiledlayout(2,2);

nexttile
[L, U, P]=lu(A);
R=P*A-L*U;
max(max(abs(R)))
norm(R, Inf)
hist(reshape(R, [n*n 1]),n)
title('Double Precision')

nexttile
[L, U, P]=lu(single(A));
L=double(L);
U=double(U);
P=double(P);
R=P*A-L*U;
max(max(abs(R)))
norm(R, Inf)
hist(reshape(R, [n*n 1]),n)
title('Single Precision')

nexttile
[L, U, P]=lu_int32(A);
R=P*A-L*U;
max(max(abs(R)))
norm(R, Inf)
hist(reshape(R, [n*n 1]),n)
title('INT32')

nexttile
[L, U, P]=lu_int32_left(A);
R=P*A-L*U;
max(max(abs(R)))
norm(R, Inf)
hist(reshape(R, [n*n 1]),n)
title('INT16')

t.TileSpacing = 'compact';
t.Padding = 'compact';
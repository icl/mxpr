function [L, U, P, C]=lu_int1632_left(A)

n=size(A,1);

scale_U = 2^30;
scale_L = 2^30;
A_int = int32(A*scale_U);
P=eye(n);
C=eye(n);
S=zeros(n);
for i=1:n
    
    for j=1:i-1
        A_int(j+1:n, i) = A_int(j+1:n, i) - (A_int(j, i)/2^16) * (A_int(j+1:n, j)/2^16) / (scale_L/2^32) ;
        if( max(abs(A_int(j+1:n, i)))>scale_U )
            A_int(:, i) = A_int(:, i) / 2;
            C(i,i) = C(i,i) * 2;
            S(i,j) = 1;
        end
    end
    
    [~, j] = max(abs(A_int(i:n,i)));
    j=j+i-1;
    
    %swap rows
    if i~=j
        temp = A_int(i,:);
        A_int(i,:) = A_int(j,:);
        A_int(j,:) = temp;
        temp = P(i,:);
        P(i,:) = P(j,:);
        P(j,:) = temp;
    end
    
    scalar = int32(2^30) / int32(A_int(i, i)/2^16);
    A_int(i+1:n, i) = (scalar * (A_int(i+1:n, i)/2^16) / int32(2^30/scale_L));
    
    %scalar = int64(2^62) / int64(A_int(i, i));
    %A_int(i+1:n, i) = int32(scalar * int64(A_int(i+1:n, i)) / int64(2^62/scale_L));
    %A_int(i+1:n, i) = int32( int64(A_int(i+1:n, i))*int64(scale_L)/int64(A_int(i, i)) );
    
    %A_int(i+1:n, i+1:n) = A_int(i+1:n, i+1:n) - A_int(i+1:n, i) * A_int(i, i+1:n); 
    
end

L = tril(A_int,-1)+scale_L*int32(eye(n));
U = triu(A_int);
L=double(L) / double(scale_L);
U=double(U) / double(scale_U);

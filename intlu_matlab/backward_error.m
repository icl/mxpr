rng(0);
n=1000;
A=rand(n);
%A=2*rand(n)-1;
%A = gallery('rando',n,2);
%A=gallery('randsvd',n,1e4,3,n-1, n-1, 1);
%A=gallery('randsvd',n,1e8,3,n-1, n-1, 1);
A=A/(max(max(A)));
b=rand(n,1);
tic;
[L U P] = lu(A);toc;
x =  U\(L\(P*b));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P] = lu(single(A));toc;
L=double(L);
U=double(U);
P=double(P);
x =  U\(L\(P*b));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P] = lu_half(half(A));toc;
x =  U\(L\(P*b));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P] = lu_int32(A);toc;
x =  U\(L\(P*b));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P C] = lu_int32_left(A);toc;
x =  ((U*C)\(L\(P*b)));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P] = lu_int16(A);toc;
x =  U\(L\(P*b));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P C] = lu_int16_left(A);toc;
x =  ((U*C)\(L\(P*b)));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P] = lu_int1632(A);toc;
x =  U\(L\(P*b));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
tic;
[L U P C] = lu_int1632_left(A);toc;
x =  ((U*C)\(L\(P*b)));
norm(A*x-b,'inf')/(norm(A,'inf')*norm(x,'inf')+norm(b,'inf'))
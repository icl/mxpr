rng(0);
n=1000;
cond_num = [1e2 1e3 1e4 1e5 1e6 1e7 1e8 1e9];
b=rand(n,1);

t = tiledlayout(5,1);

nexttile
errors1 = zeros(8, 102);
i = 1;
for c = cond_num
    rng(0);
    A=gallery('randsvd',n,c,3,n-1, n-1, 1);
    A=A/(max(max(A)));
    [L, U, P]=lu(single(A));
    L=double(L);
    U=double(U);
    P=double(P);
    tol = 1e-15;
    max_it = 1;
    restart = 100;
    x0 =  U\(L\(P*b));
    [x, error, its, flag] = gmres_sd( P*A, x0,  P*b, L, U, restart, max_it, tol);
    %[x,flag,relres,iter,error] = gmres(P*A,P*b,restart,tol,max_it,L,U,x0);
    errors1(i,:) = [error zeros(1,102-size(error, 2))];
    i=i+1;
end
semilogy(errors1')

nexttile
errors2 = zeros(8, 102);
i = 1;
for c = cond_num
    rng(0);
    A=gallery('randsvd',n,c,3,n-1, n-1, 1);
    A=A/(max(max(A)));
    [L, U, P]=lu_half(half(A));
    L=double(L);
    U=double(U);
    P=double(P);
    tol = 1e-15;
    max_it = 1;
    restart = 100;
    x0 =  U\(L\(P*b));
    [x, error, its, flag] = gmres_sd( P*A, x0,  P*b, L, U, restart, max_it, tol);
    errors2(i,:) = [error zeros(1,102-size(error, 2))];
    i=i+1;
end
semilogy(errors2')




nexttile
errors3 = zeros(8, 102);
i = 1;
for c = cond_num
    rng(0);
    A=gallery('randsvd',n,c,3,n-1, n-1, 1);
    A=A/(max(max(A)));
    [L, U, P, C]=lu_int32_left(A);
    U=U*C;
    tol = 1e-15;
    max_it = 1;
    restart = 100;
    x0 =  U\(L\(P*b));
    [x, error, its, flag] = gmres_sd( P*A, x0,  P*b, L, U, restart, max_it, tol);
    errors3(i,:) = [error zeros(1,102-size(error, 2))];
    i=i+1;
end
semilogy(errors3')



nexttile
errors4 = zeros(8, 102);
i = 1;
for c = cond_num
    rng(0);
    A=gallery('randsvd',n,c,4,n-1, n-1, 1);
    A=A/(max(max(A)));
    [L, U, P, C]=lu_int16_left(A);
    U=U*C;
    tol = 1e-15;
    max_it = 1;
    restart = 100;
    x0 =  U\(L\(P*b));
    [x, error, its, flag] = gmres_sd( P*A, x0,  P*b, L, U, restart, max_it, tol);
    errors4(i,:) = [error zeros(1,102-size(error, 2))];
    i=i+1;
end
semilogy(errors4')

nexttile
errors5 = zeros(8, 102);
i = 1;
for c = cond_num
    rng(0);
    A=gallery('randsvd',n,c,3,n-1, n-1, 1);
    A=A/(max(max(A)));
    [L, U, P, C]=lu_int1632_left(A);
    U=U*C;
    tol = 1e-15;
    max_it = 1;
    restart = 100;
    x0 =  U\(L\(P*b));
    [x, error, its, flag] = gmres_sd( P*A, x0,  P*b, L, U, restart, max_it, tol);
    errors5(i,:) = [error zeros(1,102-size(error, 2))];
    i=i+1;
end
semilogy(errors5')


set(gcf, 'Position', [100 100 1000 600]);
t = tiledlayout(1,5);
xlabel(t,'Iteration')
ylabel(t,'Estiamted residual')
idx = 0:20;
idx = repmat(idx, [8 1]);
nexttile
semilogy(idx', errors1(:, 1:21)')
title('Single Precision')
nexttile
semilogy(idx', errors2(:, 1:21)')
title('Half Precision')
nexttile
semilogy(idx', errors3(:, 1:21)')
title('INT32')
nexttile
semilogy(idx', errors4(:, 1:21)')
title('INT16')
nexttile
semilogy(idx', errors5(:, 1:21)')
title('INT16\_ACC32')
Lgnd = legend({'cond(A)=10^2', 'cond(A)=10^3', 'cond(A)=10^4', 'cond(A)=10^5', 'cond(A)=10^6', 'cond(A)=10^7', 'cond(A)=10^8', 'cond(A)=10^9'},'Location','EastOutside');
Lgnd.Position(1) = 0.01;
Lgnd.Position(2) = 0.4;
t.TileSpacing = 'compact';
t.Padding = 'compact';

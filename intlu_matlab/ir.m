function [x, error] = ir(A, x, b, L, U, max_it, tol)

normA = norm(A,'inf');
normb = norm(b,'inf');
error(1) = norm(A*x-b,'inf')/(normA*norm(x,'inf')+normb);
if ( error(1) < tol ) return, end

for iter = 1:max_it
    rtmp = b-A*x;
    r = U\(L\rtmp);
    x = x + r;
    n = norm(A*x-b,'inf')/(normA*norm(x,'inf')+normb);
    error = [error,n];                        % check convergence
    if ( error(end) <= tol ), break, end
end


end


function [L, U, P]=lu_int32(A)

n=size(A,1);

scale_U = 2^28;
scale_L = 2^30;
A_int = int32(A*scale_U);
P=eye(n);

for i=1:n
    [~, j] = max(abs(A_int(i:n,i)));
    j=j+i-1;
    
    %swap rows
    if i~=j
        temp = A_int(i,:);
        A_int(i,:) = A_int(j,:);
        A_int(j,:) = temp;
        temp = P(i,:);
        P(i,:) = P(j,:);
        P(j,:) = temp;
    end
    
    scalar = int64(2^62) / int64(A_int(i, i));
    A_int(i+1:n, i) = int32(scalar * int64(A_int(i+1:n, i)) / int64(2^62/scale_L));
    %A_int(i+1:n, i) = int32( int64(A_int(i+1:n, i))*int64(scale_L)/int64(A_int(i, i)) );
    
    %A_int(i+1:n, i+1:n) = A_int(i+1:n, i+1:n) - A_int(i+1:n, i) * A_int(i, i+1:n);
    for j=i+1:n
        A_int(j, i+1:n) = A_int(j, i+1:n) - int32(int64(A_int(j, i)) * int64(A_int(i, i+1:n)) / 2^32*int64(2^32/scale_L));
    end
    
    
end

L = tril(A_int,-1)+scale_L*int32(eye(n));
U = triu(A_int);
L=double(L) / double(scale_L);
U=double(U) / double(scale_U);

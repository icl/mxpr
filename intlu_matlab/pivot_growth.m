set(gcf, 'Position', [100 100 1000 600]);
n=2000;
A=rand(n);
c=jet(5)
[L, U, P] = lu(A);
scatter(1:n,max(abs(U)),'.','MarkerFaceColor',c(1,:),'DisplayName','A1=uniform(0,1)')
hold on
A=2*rand(n)-1;
[L, U, P] = lu(A);
scatter(1:n,max(abs(U)),'.','MarkerFaceColor',c(2,:),'DisplayName','A2=uniform(-1,1)')
A = gallery('rando',n,2);
[L, U, P] = lu(A);
scatter(1:n,max(abs(U)),'.','MarkerFaceColor',c(3,:),'DisplayName','A3=rand\{-1,1\}')

A=gallery('randsvd',n,1e4,3,n-1, n-1, 1);
A=A/max(max(A));
[L, U, P] = lu(A);
scatter(1:n,max(abs(U)),'.','MarkerFaceColor',c(4,:),'DisplayName','A4=gallery(''randsvd'',n,1e4')

A=gallery('randsvd',n,1e8,3,n-1, n-1, 1);
A=A/max(max(A));
[L, U, P] = lu(A);
scatter(1:n,max(abs(U)),'.','MarkerFaceColor',c(5,:),'DisplayName','A5=gallery(''randsvd'',n,1e8')
legend
ylabel('Maximum value in the column')
xlabel('Column index')
grid on
exportgraphics(gcf,'pivot_growth.png','Resolution',600)
hold off

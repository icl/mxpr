function [L, U, P, C]=lu_int16_left(A)

n=size(A,1);

scale_U = 2^14;
scale_L = 2^14;
A_int = int16(A.*double(scale_U));
P=eye(n);

C = eye(n);

for i=1:n
    
    for j=1:i-1
        A_int(j+1:n, i) = A_int(j+1:n, i) - int16(int32(A_int(j, i)) * int32(A_int(j+1:n, j)) / int32(scale_L) );
        if( max(abs(A_int(j+1:n, i)))>scale_U )
            A_int(:, i) = A_int(:, i) / 2;
            C(i,i) = C(i,i) * 2;
        end
    end
    
    
    [~, j] = max(abs(A_int(i:n,i)));
    j=j+i-1;
    
    %swap rows
    if i~=j
        temp = A_int(i,:);
        A_int(i,:) = A_int(j,:);
        A_int(j,:) = temp;
        temp = P(i,:);
        P(i,:) = P(j,:);
        P(j,:) = temp;
    end
    
    scalar = int32(2^30) / int32(A_int(i, i));
    A_int(i+1:n, i) = int16(scalar * int32(A_int(i+1:n, i)) / int32(2^30/scale_L));
    %A_int(i+1:n, i) = int16( int32(A_int(i+1:n, i))*int32(scale_L)/int32(A_int(i, i)) );
    
    %A_int(i+1:n, i+1:n) = A_int(i+1:n, i+1:n) - A_int(i+1:n, i) * A_int(i, i+1:n);

    

end

L = tril(A_int,-1);
U = triu(A_int);
L=double(L) / scale_L+eye(n);
U=double(U) / scale_U;

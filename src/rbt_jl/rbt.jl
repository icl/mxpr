
# functions for working with recursive butterfly transforms

"""
Computes a random, partial, recursive butterfly transforms of the given size.
The elements are generated based on `exp(x/10)` for a random `x` in `[-0.5, 0.5]`.
Note that this is not the only way to generate the transforms.
"""
function make_rbt(T, m, n; d=4)
    u = rand(T, m, d)
    v = rand(T, n, d)
    @. u = exp((u-.5)/10)
    @. v = exp((v-.5)/10)

    return u,v
end

"""
Applies the given packed butterfly transform in place to the given vector.
"""
function rbt_apply!(b::AbstractVector, transpose::Val{false}, u::AbstractMatrix)
    d = size(u, 2)
    n = size(u, 1)
    @boundscheck @assert n == size(b, 1)

    if d == 0
        return b
    end

    # Note that the transform is applied one recursive layer at a time,
    # each layer is applied one butterfly at a time, and
    # each butterfly is applied as a 2x2 submatrix

    # For each recursion depth
    for k in 1:d
        num_butterflies = 2^(k-1)
        len = div(n, 2*num_butterflies)

        # For each butterfly in the current recursion depth
        for i in 1:num_butterflies
            offset = (i-1)*2*len

            # For each element in half of the current butterfly
            @inbounds for j in 1:len
                ind_1 = offset + j
                ind_2 = ind_1 + len

                # do the submatrix-vector multiplication
                a1 = u[ind_1, k]*b[ind_1];
                a2 = u[ind_2, k]*b[ind_2];

                b[ind_1] = a1+a2;
                b[ind_2] = a1-a2;
            end
        end
    end

    return b
end
function rbt_apply!(b::AbstractMatrix, transpose::Val{false}, u::AbstractMatrix)
    d = size(u, 2)
    n = size(u, 1)
    @boundscheck @assert n == size(b, 1)

    cols = size(b, 2)

    if d == 0
        return b
    end

    # Note that the transform is applied one recursive layer at a time,
    # each layer is applied one butterfly at a time, and
    # each butterfly is applied as a 2x2 submatrix

    # For each recursion depth
    for k in 1:d
        num_butterflies = 2^(k-1)
        len = div(n, 2*num_butterflies)

        # For each butterfly in the current recursion depth
        for i in 1:num_butterflies
            offset = (i-1)*2*len

            # For each element in half of the current butterfly
            @inbounds for j in 1:len
                ind_1 = offset + j
                ind_2 = ind_1 + len

                for c in 1:cols
                    # do the submatrix-vector multiplication
                    a1 = u[ind_1, k]*b[ind_1, c];
                    a2 = u[ind_2, k]*b[ind_2, c];

                    b[ind_1, c] = a1+a2;
                    b[ind_2, c] = a1-a2;
                end
            end
        end
    end

    return b
end


function rbt_apply!(b::AbstractVector, transpose::Val{true}, u::AbstractMatrix)
    d = size(u, 2)
    n = size(u, 1)
    @boundscheck @assert n == size(b, 1)

    if d == 0
        return b
    end

    # Note that the transform is applied one recursive layer at a time,
    # each layer is applied one butterfly at a time, and
    # each butterfly is applied as a 2x2 submatrix

    # For each recursion depth
    for k in d:-1:1
        num_butterflies = 2^(k-1)
        len = div(n, 2*num_butterflies)

        # For each butterfly in the current recursion depth
        for i in 1:num_butterflies
            offset = (i-1)*2*len

            # For each element in half of the current butterfly
            @inbounds for j in 1:len
                ind_1 = offset + j
                ind_2 = ind_1 + len

                # do the submatrix-vector multiplication

                a1 = b[ind_1] + b[ind_2]
                a2 = b[ind_1] - b[ind_2]

                b[ind_1] = u[ind_1, k]*a1
                b[ind_2] = u[ind_2, k]*a2
            end
        end
    end

    return b
end
function rbt_apply!(b::AbstractMatrix, transpose::Val{true}, u::AbstractMatrix)
    d = size(u, 2)
    n = size(u, 1)
    @boundscheck @assert n == size(b, 1)

    cols = size(b, 2)

    if d == 0
        return b
    end

    # Note that the transform is applied one recursive layer at a time,
    # each layer is applied one butterfly at a time, and
    # each butterfly is applied as a 2x2 submatrix

    # For each recursion depth
    for k in d:-1:1
        num_butterflies = 2^(k-1)
        len = div(n, 2*num_butterflies)

        # For each butterfly in the current recursion depth
        for i in 1:num_butterflies
            offset = (i-1)*2*len

            # For each element in half of the current butterfly
            @inbounds for j in 1:len
                ind_1 = offset + j
                ind_2 = ind_1 + len

                # do the submatrix-vector multiplication
                for c in 1:cols
                    a1 = b[ind_1, c] + b[ind_2, c]
                    a2 = b[ind_1, c] - b[ind_2, c]

                    b[ind_1, c] = u[ind_1, k]*a1
                    b[ind_2, c] = u[ind_2, k]*a2
                end
            end
        end
    end

    return b
end



"""
Applies the given packed butterfly transforms on the left and right respectively.
Currently, `A` must be square.
"""
function rbt_rbt_apply!(A::AbstractMatrix, u::AbstractMatrix, v::AbstractMatrix)
    d = size(u, 2)
    n = size(u, 1)
    @boundscheck begin
        @assert n == size(A, 1)
        @assert n == size(A, 2)
        @assert d == size(v, 2)
    end

    if d == 0
        return A
    end

    # Note that the transform is applied one recursive layer at a time,
    # each layer is applied one butterfly at a time, and
    # each butterfly is applied as a 2x2 submatrix.

    # Additionally, because transforms are applied on both sides, the cartesian
    # product of the two sides' butterflies and submatrices must be iterated over.

    # For each recursion depth
    for k in d:-1:1
        num_butterflies = 2^(k-1)
        len = div(n, 2*num_butterflies);

        # For each combination of u and v butterflies in the current recursion depth
        for i1 in 1:num_butterflies
            offset_u = (i1-1)*2*len
            for i2 in 1:num_butterflies
                offset_v = (i2-1)*2*len

                # For each element in half of the current butterfly combination
                @inbounds for j1 in 1:len
                    ind_u_1 = offset_u + j1
                    ind_u_2 = ind_u_1 + len

                    u1 = u[ind_u_1, k]
                    u2 = u[ind_u_2, k]

                    for j2 in 1:len
                        ind_v_1 = offset_v + j2
                        ind_v_2 = ind_v_1 + len

                        v1 = v[ind_v_1, k]
                        v2 = v[ind_v_2, k]


                        # do the submatrix multiplication
                        a00 = A[ind_u_1, ind_v_1]
                        a01 = A[ind_u_1, ind_v_2]
                        a10 = A[ind_u_2, ind_v_1]
                        a11 = A[ind_u_2, ind_v_2]

                        b1 = a00 + a01
                        b2 = a10 + a11
                        b3 = a00 - a01
                        b4 = a10 - a11

                        A[ind_u_1, ind_v_1] = u1*v1*(b1+b2)
                        A[ind_u_1, ind_v_2] = u1*v2*(b3+b4)
                        A[ind_u_2, ind_v_1] = u2*v1*(b1-b2)
                        A[ind_u_2, ind_v_2] = u2*v2*(b3-b4)
                    end
                end
            end
        end
    end

    return A
end

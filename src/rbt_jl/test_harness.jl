
import Base.Filesystem
import CSV
import DataFrames
using LinearAlgebra
import Random

include("solvers.jl")

lu_piv(A, b; refine=false, info=Dict())   = gesv!(A, b; pivot=true, refine=refine, info=info)
lu_nopiv(A, b; refine=false, info=Dict()) = gesv!(A, b; pivot=false, refine=refine, info=info)
rbt_1(A, b; refine=false, info=Dict())    = gesv_rbt!(A, b; d=1, refine=refine, info=info)
rbt_2(A, b; refine=false, info=Dict())    = gesv_rbt!(A, b; d=2, refine=refine, info=info)
rbt_3(A, b; refine=false, info=Dict())    = gesv_rbt!(A, b; d=3, refine=refine, info=info)
rbt_4(A, b; refine=false, info=Dict())    = gesv_rbt!(A, b; d=4, refine=refine, info=info)
rbt_5(A, b; refine=false, info=Dict())    = gesv_rbt!(A, b; d=5, refine=refine, info=info)
rbt_6(A, b; refine=false, info=Dict())    = gesv_rbt!(A, b; d=6, refine=refine, info=info)


function run_comparison(generate_system, sizes;
                        refine=false,
                        solvers = (lu_piv, lu_nopiv, rbt_1, rbt_2, rbt_3, rbt_4),
                        show_progress=true)

    results = DataFrames.DataFrame(solver=String[], size=UInt64[], refine=Int[],
                                   time=Float64[], alloc=UInt64[],
                                   normwise_backwards_error=Float64[], relative_residual=Float64[],
                                   refinements=UInt[])
    errors = DataFrames.DataFrame(solver=String[], size=UInt64[], refine=Int[],
                                  error = String[])

    if show_progress
        println("_"^(length(sizes)*length(solvers)))
    end

    for n in sizes
        A,b = generate_system(n)

        b_norm = norm(b)
        A_norm = norm(A)

        for sv in solvers
            A_copy = copy(A)
            b_copy = copy(b)

            Random.seed!(100)

            try
                info = Dict()
                metrics = @timed sv(A_copy, b_copy; refine=refine, info=info)
                res_norm = norm(b - A*b_copy)
                backErr = res_norm/(b_norm + A_norm*norm(b_copy))
                relRes = res_norm/b_norm

                # solver, size, iterative_refinement_p, exec_time, bytes_allocated, normwise_backwards_error
                push!(results, (string(sv), n, refine,
                                metrics[2], metrics[3],
                                backErr, relRes,
                                get(info, "refinements", 0)))
                if show_progress
                    print("#")
                end
            catch e
                push!(errors, (string(sv), n, refine, string(e)))

                if show_progress
                    print("x")
                end
            end
        end
    end
    if show_progress
        println()
    end
    return results, errors
end


function uniform_random_system(n)
    Random.seed!(n)
    b = rand(Float64, n)
    A = rand(Float64, n, n)
    return A,b
end

function generate_system_for_k(n, k)
    Random.seed!(n)
    b = rand(Float64, n)

    A = rand(Float64, n, n)
    inc = (1-1/k)/(n-1)
    for i in 1:n
        A[i, i] = (i-1)*inc + 1/k
    end

    return A,b
end

function random_k2_system(n)
    generate_system_for_k(n, 2)
end

function random_k1e8_system(n)
    generate_system_for_k(n, 1e8)
end

function random_k1e16_system(n)
    generate_system_for_k(n, 1e16)
end

function eye_system(n)
    A = Matrix{Float64}(I, n, n)
    Random.seed!(n)
    b = rand(Float64, n)
    return A,b
end


function save_results(test, results)
    Filesystem.mkpath("results/")
    CSV.write("results/"*test*".csv", results)
end

function plot_results_from_file(test)
    results = CSV.read("results/"*test*".csv")

    plot_comparison_results(DataFrames.eachrow(results), nothing)
    savefig("results/"*test*"-n-normwise_backwards_error.png")
    plot_comparison_results(DataFrames.eachrow(results), nothing; y="relative_residual")
    savefig("results/"*test*"-n-relative_residual.png")
end

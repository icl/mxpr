
# Basic unit tests

using Test

include("rbt.jl")


@testset "rbt_rbt_apply! tests" begin
    u = [1 1; 1 1; 1 1; 1 1]
    v = [1 1; 1 1; 1 1; 1 1]
    A = Float64[9 6 5 5; 0 1 0 6; 7 8 2 7; 9 7 6 8]

    rbt_rbt_apply!(A, u, v)
    @test A == Float64[ 86 -10  8  16;
                        12   4 14  -2;
                       -22   2 -8   0;
                        24  16  6  -2]
    @test u == [1 1; 1 1; 1 1; 1 1]
    @test v == [1 1; 1 1; 1 1; 1 1]

    A = Float64[9 6 5 5; 0 1 0 6; 7 8 2 7; 9 7 6 8]
    u = zeros(Float64, 4, 0)
    v = zeros(Float64, 4, 0)
    rbt_rbt_apply!(A, u, v)
    @test A == Float64[9 6 5 5; 0 1 0 6; 7 8 2 7; 9 7 6 8]
    @test u == zeros(Float64, 4, 0)
    @test v == zeros(Float64, 4, 0)


    u = [1.125 0.875; 0.875 1.125; 1.125 0.875; 0.875 1.125]
    v = [0.875 1.125; 1.125 0.875; 0.875 1.125; 1.125 0.875]
    A = Float64[9 6 5 5; 0 1 0 6; 7 8 2 7; 9 7 6 8]
    rbt_rbt_apply!(A, u, v)
    # Computed via Mathematica
    @test A == [170667/2048 -(19845/2048)  3969/512  3969/256;
               11907/1024  3969/1024  27783/2048  -(3969/2048);
               -(43659/2048)  3969/2048  -(3969/512)  0;
               11907/512  3969/256  11907/2048 -(3969/2048)]
    @test u == [1.125 0.875; 0.875 1.125; 1.125 0.875; 0.875 1.125]
    @test v == [0.875 1.125; 1.125 0.875; 0.875 1.125; 1.125 0.875]
end


@testset "rbt_apply! vect,trans tests" begin
    u = zeros(Float64, 4, 0)
    b = Float64[25, 7, 24, 30]
    rbt_apply!(b, Val(true), u)
    @test b == Float64[25, 7, 24, 30]
    @test u == zeros(Float64, 4, 0)

    u = [1 1; 1 1; 1 1; 1 1]
    b = Float64[25, 7, 24, 30]
    rbt_apply!(b, Val(true), u)
    @test b == [86, 12, -22, 24]
    @test u == [1 1; 1 1; 1 1; 1 1]

    u =  [1.125 0.875; 0.875 1.125; 1.125 0.875; 0.875 1.125]
    b = Float64[25, 7, 24, 30]
    rbt_apply!(b, Val(true), u)
    # Computed via Mathematica
    @test b == [2709/32, 189/16, -693/32, 189/8]
    @test u == [1.125 .875; .875 1.125; 1.125 .875; .875 1.125]
end

@testset "rbt_apply! multivect,trans tests" begin
    u = zeros(Float64, 4, 0)
    b = Float64[25 -5; 7 1; 24 16; 30 -2.5]
    rbt_apply!(b, Val(true), u)
    @test b == Float64[25 -5; 7 1; 24 16; 30 -2.5]
    @test u == zeros(Float64, 4, 0)

    u = [1 1; 1 1; 1 1; 1 1]
    b = Float64[25 -5; 7 1; 24 16; 30 -2.5]
    rbt_apply!(b, Val(true), u)
    @test b == [86 9.5; 12 12.5; -22 -17.5; 24 -24.5]
    @test u == [1 1; 1 1; 1 1; 1 1]

    u =  [1.125 0.875; 0.875 1.125; 1.125 0.875; 0.875 1.125]
    b = Float64[25 -5; 7 1; 24 16; 30 -2.5]
    rbt_apply!(b, Val(true), u)
    # Computed via Mathematica
    @test b == [2709/32 1197/128; 189/16 1575/128; -693/32 -2205/128; 189/8 -3087/128]
    @test u == [1.125 .875; .875 1.125; 1.125 .875; .875 1.125]
end

@testset "rbt_apply! vect,notrans tests" begin
    u = zeros(Float64, 4, 0)
    b = Float64[25, 7, 24, 30]
    rbt_apply!(b, Val(false), u)
    @test b == Float64[25, 7, 24, 30]
    @test u == zeros(Float64, 4, 0)

    u = [1 1; 1 1; 1 1; 1 1]
    b = Float64[25, 7, 24, 30]
    rbt_apply!(b, Val(false), u)
    @test b == [86, 12, -22, 24]
    @test u == [1 1; 1 1; 1 1; 1 1]

    u =  [1.125 0.875; 0.875 1.125; 1.125 0.875; 0.875 1.125]
    b = Float64[25, 7, 24, 30]
    rbt_apply!(b, Val(false), u)
    # Computed via Mathematica
    @test b == [2709/32, 189/16, -693/32, 189/8]
    @test u == [1.125 .875; .875 1.125; 1.125 .875; .875 1.125]


    u = [0.875 1.125; 1.125 0.875; 0.875 1.125; 1.125 0.875]
    b = [1.0158730158730158, 0.0, -0.0, -0.0]
    rbt_apply!(b, Val(false), u)
    @test isapprox(b, [1.0, 1.0, 1.0, 1.0]; rtol=0, atol=1e-16)
end

@testset "rbt_apply! multivect,notrans tests" begin
    u = zeros(Float64, 4, 0)
    b = Float64[25 -5; 7 1; 24 16; 30 -2.5]
    rbt_apply!(b, Val(false), u)
    @test b == Float64[25 -5; 7 1; 24 16; 30 -2.5]
    @test u == zeros(Float64, 4, 0)

    u = [1 1; 1 1; 1 1; 1 1]
    b = Float64[25 -5; 7 1; 24 16; 30 -2.5]
    rbt_apply!(b, Val(false), u)
    @test b == [86 9.5; 12 12.5; -22 -17.5; 24 -24.5]
    @test u == [1 1; 1 1; 1 1; 1 1]

    u =  [1.125 0.875; 0.875 1.125; 1.125 0.875; 0.875 1.125]
    b = Float64[25 -5; 7 1; 24 16; 30 -2.5]
    rbt_apply!(b, Val(false), u)
    # Computed via Mathematica
    @test b == [2709/32 1197/128; 189/16 1575/128; -693/32 -2205/128; 189/8 -3087/128]
    @test u == [1.125 .875; .875 1.125; 1.125 .875; .875 1.125]

    u = [0.875 1.125; 1.125 0.875; 0.875 1.125; 1.125 0.875]
    b = [1.0158730158730158 -1.0158730158730158; 0.0 -0.0; -0.0 0.0; -0.0 0.0]
    rbt_apply!(b, Val(false), u)
    @test isapprox(b, [1.0 -1.0; 1.0 -1.0; 1.0 -1.0; 1.0 -1.0]; rtol=0, atol=1e-16)
end

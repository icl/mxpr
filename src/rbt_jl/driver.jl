
# A test harness for comparing varients of lu

using ArgParse


s = ArgParseSettings()
@add_arg_table! s begin
    "--refine"
        help = "The maximum number of refinement iterations"
        action = :append_arg
        arg_type = Int
        default = []
    "--plot"
        help = "If the results should be plotted"
        action = :store_true
    "--cond"
        help = "If the condition numbers should be computed and saved"
        action = :store_true
    "--warm-code"
        help = "Whether to do untimed tests to warm the code"
        action = :store_true
end
parsed_args = parse_args(ARGS, s)

if parsed_args["warm-code"]
    println("Warming code")
    run_comparison(uniform_random_system, (4, 32, 512))
    run_comparison(uniform_random_system, (4, 32, 512); refine=true)
end

sizes = (4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048)
include("test_harness.jl")
if parsed_args["plot"]
    include("result_plotter.jl")
end

generators = ("uniform_random", "random_k2", "random_k1e8", "random_k1e16")

refine = parsed_args["refine"]

if parsed_args["cond"]
    conditions = Tuple[("size", [string(s) for s in sizes]...)]
end
for gen_name in generators

    if parsed_args["cond"]
        # compute conditions
        generator_conditions = Float64[]
        for n in sizes
            A, _ = gen(n)
            push!(generator_conditions, cond(A))
        end
        push!(conditions, tuple(gen_name, generator_conditions...))
    end

    for refine in parsed_args["refine"]
        gen = eval(Symbol(gen_name*"_system"))
        test_desc = gen_name*"-refine="*string(refine)
        println(" Testing "*test_desc)

        results, errors = run_comparison(gen, sizes; refine=refine)
        if DataFrames.nrow(errors) != 0
            println("Errors:")
            for err in DataFrames.eachrow(errors)
                println(" $(err.solver) n=$(err.size) refine=$(err.refine) $(err.error)")
            end
        end
        save_results(test_desc, results)

        if parsed_args["plot"]
            if parsed_args["cond"]
                conditions = (sizes, generator_conditions)
            else
                conditions = nothing
            end
            plot_comparison_results(DataFrames.eachrow(results), conditions)
            savefig("results/"*test_desc*"-n-normwise_backwards_error.png")
            plot_comparison_results(DataFrames.eachrow(results), conditions; y="relative_residual")
            savefig("results/"*test_desc*"-n-relative_residual.png")
        end
    end

end
if parsed_args["cond"]
    Filesystem.mkpath("results/")
    CSV.write("results/conditions.csv", conditions, writeheader=false)
end

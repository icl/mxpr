
using LinearAlgebra
import Random

include("rbt.jl")

F77_INT = Int32

F_EPS = 0.0
begin
    F_EPS = ccall(("dlamch_", "libLAPACK-tester"),
                  Float64,
                  (Ptr{UInt8},), "Epsilon")
    println("eps = $F_EPS")
end

function generate_matrix(imat, n, iseed)

    path = "DGE"

    type = "N"
    kl = Ref{F77_INT}(0)
    ku = Ref{F77_INT}(0)
    anorm = Ref{Float64}(0.0)
    mode = Ref{F77_INT}(0)
    cndnum = Ref{Float64}(0.0)
    dist = "S"

    n_fort = F77_INT(n)
    imat_fort = F77_INT(imat)

    ccall(("dlatb4_", "libLAPACK-tester"),
          Cvoid,
          (Ptr{UInt8}, Ref{F77_INT}, Ref{F77_INT}, Ref{F77_INT},
           Ptr{UInt8}, Ref{F77_INT}, Ref{F77_INT}, Ref{Float64}, Ref{F77_INT}, Ref{Float64}, Ptr{UInt8}),
          path, imat_fort, n_fort, n_fort,
          type, kl, ku, anorm, mode, cndnum, dist)

    rwork = zeros(Float64, n)
    work = zeros(Float64, 3*n)
    pack = "N"
    A = zeros(Float64, n, n)
    info = Ref{F77_INT}(0)
    ccall(("dlatms_", "libLAPACK-tester"),
          Cvoid,
          (Ref{F77_INT}, Ref{F77_INT}, Ptr{UInt8}, Ptr{F77_INT}, Ptr{UInt8}, Ptr{Float64}, Ref{F77_INT},
           Ref{Float64}, Ref{Float64}, Ref{F77_INT}, Ref{F77_INT}, Ptr{UInt8}, Ptr{Float64}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT}),
          n_fort, n_fort, dist, iseed, type, rwork, mode,
          cndnum, anorm, kl, ku, pack, A, n_fort,
          work, info)
    if info[] != 0
        error("DLATMS failed with error code $info")
    end

    return (A, cndnum[], 1/cndnum[])
end

function generate_rhs(A, nrhs, iseed)

    n = F77_INT(size(A, 1))
    @assert n === F77_INT(size(A, 2))

    path = "DGE"
    xtype = "N"
    uplo = "F"
    trans = "N"
    bandwidth = n - F77_INT(1)

    x = zeros(Float64, n, nrhs)
    b = zeros(Float64, n, nrhs)

    info = Ref{F77_INT}(0)

    ccall(("dlarhs_", "libLAPACK-tester"),
          Cvoid,
          (Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8}, Ptr{UInt8},
           Ref{F77_INT}, Ref{F77_INT}, Ref{F77_INT}, Ref{F77_INT}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT}, Ptr{Float64}, Ref{F77_INT}, Ptr{Float64}, Ref{F77_INT},
           Ptr{F77_INT}, Ref{F77_INT}),
          path, xtype, uplo, trans,
          n, n, bandwidth, bandwidth, nrhs,
          A, n, x, n, b, n,
          iseed, info)

    if info[] != 0
        error("dlarhs failed due to an invalid argument in the $(-info)th position")
    end

    return x, b
end


function tester_solve!(A, x, solve_params)
    U = solve_params["left rbt"]
    V = solve_params["right rbt"]
    pivot = get(solve_params, "pivot", false)
    refine = get(solve_params, "refine steps", 1)

    n_in = size(A, 1)
    x_orig = x

    n = Int(2^ceil(log2(n_in)))
    if n != n_in
        A_copy = zeros(Float64, n, n)
        A_copy += I
        A_copy[1:n_in, 1:n_in] = A

        A = A_copy

        x_copy = zeros(Float64, n, size(x, 2))
        x_copy[1:n_in, :] = x
        x = x_copy
    else
        A = copy(A)
    end

    rbt_rbt_apply!(A, U, V)
    rbt_apply!(x, Val(true), U)

    if refine != 0
        rhs = copy(x)
    else
        rhs = x
    end

    LU = lu(A, Val(pivot))
    ldiv!(LU, x)

    if refine != 0
        r = rhs - A*x
        i = 1
        while i <= refine
            ldiv!(LU, r)
            @. x += r

            r .= rhs .- A*x
            i += 1
        end
    end

    rbt_apply!(x, Val(false), V)

    if x !== x_orig
        x_orig .= x[1:n_in, :]
    end

    return LU
end

function tester_LU_residual(LU, A, solve_params)
    # norm(LU - A) / (n*norm(A)*eps)
    n = size(A, 1)
    return norm((LU.L*LU.U)[1:n, 1:n] - A, 1) / (n*norm(A, 1)*F_EPS)
end

function tester_sol_residual(A, b, x, solve_params)
    # norm(b - A*x) / (norm(A)*norm(x)*eps)
    #return norm(b - A*x, 1) / (norm(A, 1)*norm(x, 1)*F_EPS)

    n = size(A, 1)

    resid = Ref(0.0)
    rwork = zeros(Float64, n)
    b_copy = copy(b)

    ccall(("dget02_", "libLAPACK-tester"),
          Cvoid,
          (Ptr{UInt8}, Ref{F77_INT}, Ref{F77_INT}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT},
           Ptr{Float64}, Ref{Float64}),
          "No transpose", n, n, size(x, 2),
          A, n,
          x, n,
          b_copy, n,
          rwork, resid)

    return resid[]
end

function lapack_cond_1(A)
    n = size(A, 1)
    # ANORMO = DLANGE( '1', N, N, AFAC, LDA, RWORK )
    anorm1 = ccall(("dlange_", "libLAPACK-tester"),
                   Float64,
                   (Ptr{UInt8}, Ref{F77_INT}, Ref{F77_INT},
                    Ptr{Float64}, Ref{F77_INT}, Ptr{Float64}),
                   "1", n, n, A, n, Float64[])

    # CALL DGETRF( N, N, AFAC, LDA, IWORK, INFO )
    ipiv = zeros(F77_INT, size(A, 1))
    info = Ref(F77_INT(0))
    afac = copy(A)
    ccall(("dgetrf_", "libLAPACK-tester"),
          Cvoid,
          (Ref{F77_INT}, Ref{F77_INT}, Ptr{Float64}, Ref{F77_INT}, Ptr{F77_INT}, Ref{F77_INT}),
          n, n, afac, n, ipiv, info)
    @assert info[] == 0

    # CALL DGETRI( N, AFAC, LDA, IWORK, WORK, LWORK, INFO )
    ccall(("dgetri_", "libLAPACK-tester"),
          Cvoid,
          (Ref{F77_INT}, Ptr{Float64}, Ref{F77_INT}, Ptr{F77_INT},
           Ptr{Float64}, Ref{F77_INT}, Ref{F77_INT}),
          n, afac, n, ipiv, zeros(Float64, n*8), n*8, info)
    @assert info[] == 0

    # AINVNM = DLANGE( '1', N, N, A, LDA, RWORK )
    ainvnorm1 = ccall(("dlange_", "libLAPACK-tester"),
                    Float64,
                    (Ptr{UInt8}, Ref{F77_INT}, Ref{F77_INT},
                     Ptr{Float64}, Ref{F77_INT}, Ptr{Float64}),
                    "1", n, n, afac, n, Float64[])

    return (1/anorm1)/ainvnorm1
end

function tester_sol_error(A, x, xact, solve_params)
    # (norm(x - xact)*rcond) / norm(xact)*eps

    #rcond1 = 1 / cond(A, 1)
    #rcond2 = (1/norm(A, 1))/norm(inv(A), 1)
    #rcond3 = (1/opnorm(A, 1))/opnorm(inv(A), 1)
    rcond = lapack_cond_1(A)

    resid = Ref(0.0)
    #return norm(x - xact, 1)*rcond / (norm(xact, 1)*F_EPS)
    ccall(("dget04_", "libLAPACK-tester"),
          Cvoid,
          (Ref{F77_INT}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT},
           Ptr{Float64}, Ref{F77_INT},
           Ref{Float64}, Ref{Float64}),
          size(x, 1), size(x, 2),
          x, size(x, 1),
          xact, size(xact, 1),
          rcond, resid)

    return resid[]
end


"""
Runs the tests for gesv

Parameters
* `solve_params` - the parameters to configure `solve!` and the error computing routines
* `imat` - the integer id of the matrix type (See LAPACK tester)
* `n` - the size of the tested system
* `nrhs` - the number of right hand sides to test
"""
function test_mat(solve_params, imat, n, nrhs; verbose=false)
    # ignore matrix types with zero rows/cols
    # They're just for testing return codes
    @assert imat != 5
    @assert imat != 6
    @assert imat != 7

    iseed = F77_INT[1988, 1989, 1990, 1991]
    (A, cndnum, rcndnum) = generate_matrix(imat, n, iseed)
    (xact, b) = generate_rhs(A, nrhs, iseed)

    cndnum = cond(A)

    x = copy(b)
    try
        LU = tester_solve!(A, x, solve_params)

        #LU_res = tester_LU_residual(LU, A, solve_params)
        #if LU_res >= solve_params["thresh"]
        #    println("imat=$imat n=$n nrhs=$nrhs  LU_res=$LU_res")
        #end

        sol_res = tester_sol_residual(A, b, x, solve_params)
        sol_err = tester_sol_error(A, x, xact, solve_params)


        if sol_res >= solve_params["thresh"] || sol_err >= solve_params["thresh"]
            if verbose
                println("imat=$imat n=$n nrhs=$nrhs cond=$cndnum sol_res=$sol_res sol_err=$sol_err")
            end
            return false
        end
    catch err
        if err isa SingularException
            if verbose
                println("imat=$imat n=$n nrhs=$nrhs cond=$cndnum  $err")
            end
            return false
        else
            rethrow()
        end
    end

    return true
end

function run_LAPACK_tester(; all_d=[0, 1, 2, 3, 4], all_refine=[0, 1, 2], all_pivot=[true, false],
                             all_imat=[1, 2, 3, 4, 8, 9],
                             all_n=[2, 3, 5, 10, 50], all_nrhs=[1, 2, 15],
                             all_seeds=[42],
                             verbose=false, threshold=30.0)
    for d in all_d
        for refine in all_refine
            for pivot in all_pivot
                println("##### d=$d  refine=$refine  pivot=$pivot #####")
                failures = 0
                for imat in all_imat
                    for n in all_n
                        n_round = Int(2^ceil(log2(n)))
                        for nrhs in all_nrhs
                            for seed in all_seeds
                                Random.seed!(seed)
                                (U, V) = make_rbt(Float64, n_round, n_round; d=d)
                                solve_params = Dict("thresh" => threshold,
                                                    "left rbt"=>U, "right rbt"=>V,
                                                    "refine steps"=>refine, "pivot"=>pivot)
                                pass = test_mat(solve_params, imat, n, nrhs; verbose=verbose)
                                if !pass
                                    failures += 1
                                end
                            end
                        end
                    end
                end
                if failures != 0
                    println("$failures tests failed ")
                end
            end
        end
    end
end

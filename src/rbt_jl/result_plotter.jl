
using Plots


function label(key)
    if key == "normwise_backwards_error"
        return "Normwise Backwards Error (||b-Ax||/(||b||+||A||*||x||))"
    elseif key == "relative_residual"
        return "Relative Residual (||b-Ax||/||b||)"
    else
        return key
    end
end

function plot_comparison_results(results, conditions;
                                 x="size",
                                 y="normwise_backwards_error")

    solvers = unique([result[:solver] for result in results])

    x_key = Symbol(x)
    y_key = Symbol(y)
    x_series = [[result[Symbol(x_key)] for result in results if result[:solver]==solver] for solver in solvers]
    y_series = [[result[Symbol(y_key)] for result in results if result[:solver]==solver] for solver in solvers]


    plot(x_series,
        y_series;
        linestyle=[:dash  :dash  fill(:solid, (1, length(solvers)-2))...],
        linecolor=[:red :green :blue RGBA(0, 0, 1.0, 1.0) RGBA(0, 0, 0.75, 1.0) RGBA(0, 0, 0.5, 1.0) RGBA(0, 0, 0.25, 1.0)],
        label=reshape(solvers, (1, :)),
        xlabel=label(x), xscale=:log10,
        ylabel=label(y), yscale=:log10, ylim=(.9*10^-17, 9),
        yticks = [10.0^i for i in -17:0],
        legend=:topleft,
        gridalpha=0.5,
        dpi=300)


    if y == "normwise_backwards_error"
        plot!([eps(Float64)], seriestype=:hline, label="refinement target", linestyle=:dot, linecolor=:black)
    end
    if conditions != nothing
        plt = twinx()
        plot!(plt, collect(conditions[1]), conditions[2],
            linecolor=:black,
            linestyle=:dashdot,
            label="condition",
            xscale=:log10,
            ylabel="Condition", yscale=:log10)
    end
end

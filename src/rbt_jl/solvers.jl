
# Wrapper functions to provide a nice solve interface over the various lu impls

using LinearAlgebra

include("rbt.jl")

"""
Factors and solves the given linear system, overwriting b with the solution
and A with the LU factors.

Iterative refinement is run until the component-wise backward error is below
epsilon for the matrix's type (i.e.  `(||r|| <= eps(T)*(||b|| + ||A||*||x||))`),
or until a number of refinement iterations equal to `refine` have been run.
The `norm` keyword allows changing the norm used in the above formula.
"""
function gesv!(A::AbstractMatrix{T}, b; pivot=true, refine=false, norm=(x->norm(x, 2)), info=Dict()) where T
    if refine > 0

        A_orig = copy(A)
        A_norm = norm(A_orig)
        rhs = copy(b)
        rhs_norm = norm(rhs)

        F = lu!(A, Val(pivot))
        ldiv!(F, b)

        r = rhs - A_orig*b
        i = 0
        while (i+1 < refine) && norm(r) > eps(T)*(rhs_norm + A_norm*norm(b))
            i += 1
            ldiv!(F, r)
            @. b += r

            r .= rhs .- A_orig*b
        end
        info["refinements"] = i
    else
        F = lu!(A, Val(pivot))
        ldiv!(F, b)
    end
    return b
end

"""
Solves the system using partial butterfly transforms, overwriting `b` with the
solution, and `A` with the LU factors of the transformed system.
"""
function gesv_rbt!(A::AbstractMatrix{T}, b; d=4, refine=false, norm=(x->norm(x, 2)), info=Dict()) where T

    m,n = size(A)
    u,v = make_rbt(T, m, n; d=d)

    rbt_rbt_apply!(A, u, v)
    rbt_apply!(b, Val(true), u)

    gesv!(A, b; pivot=false, refine=refine, norm=norm, info=info)

    return rbt_apply!(b, Val(false), v)
end

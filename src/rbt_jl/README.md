Code for experimenting with using partial, recursive, random butterfly transforms to solve systems of linear equations.

The code is primarily based on *Accelerating linear system solutions using randomization techniques*.


### Butterfly matrix format
The recursive butterfly transforms are stored in packed format, instead of as the dense matrix.
Each of the butterfly matrices making up the recursive butterfly matrix is stored as a single column vector representing the diagonal values for the first half of the matrix, then the diagonal values for the second half of the matrix.
Because the size of the butterfly matrices are inversely proportional to the number of butterfly matrices in a shared matrix factor, a recursive butterfly transform can be stored as an `n` by `d` array for a recursion depth of `d`. 
